%define build_timestamp %(date +"%Y%m%d")

Name:       labsync-tray
Version:    1.1
Release:    %{build_timestamp}
Summary:    Segnala lo stato di aggiornamento della cartella /opt sulle macchine dei laboratori Linux del DiSIT.
License:    GPLv3+
BuildArch:  noarch
BuildRoot:  %{_tmppath}/%{name}-buildroot
URL:        https://gitlab.di.unipmn.it/ulisse/%{name}
Source0:    https://gitlab.di.unipmn.it/ulisse/%{name}/-/archive/master/labsync-tray-master.tar.gz

### #/%{name}-%{version}-%{release}.tar.gz

Requires:   python2 python2-gobject libappindicator-gtk3 gtk3
#pygobject2

%description
Programma in Python che viene lanciato all'inizio della sessione grafica per segnalare lo stato di aggiornamento della cartella /opt (effettuato attraverso il servizio labsync) sulle macchine dei laboratori Linux del DiSIT.

%prep
%autosetup -n %{name}-master
#%setup -q -c

%build
# nothing to do

%install
mkdir -p %{buildroot}/usr/local/sbin/
mkdir -p %{buildroot}/etc/xdg/autostart/
install -m 755 %{_builddir}/%{name}-master/src/labsync-tray.py %{buildroot}/usr/local/sbin/labsync-tray.py
install -m 644 %{_builddir}/%{name}-master/src/labsync-tray.desktop %{buildroot}/etc/xdg/autostart/labsync-tray.desktop

%clean
rm -rf %{buildroot}

%files
/usr/local/sbin/labsync-tray.py
/etc/xdg/autostart/labsync-tray.desktop

%post
# nothing to do

%changelog
* Mon Jun 24 2019 Alberto Livio Beccaria <alberto.liviobeccaria@uniupo.it>
- Initial spec file and package into RPM
