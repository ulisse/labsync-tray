import sys
from typing import Dict
from PyQt6.QtWidgets import QApplication, QSystemTrayIcon, QStyle, QMenu
from PyQt6.QtCore import QObject, pyqtSignal, QTimer
from PyQt6.QtGui import QIcon, QAction


class SysTray(QObject):
    def __init__(self, app: QApplication) -> None:
        super().__init__(parent=None)

        self.systray = QSystemTrayIcon()
        if not self.systray.isSystemTrayAvailable():
            print("System tray not available for this system")
            return

        icon = QIcon("icon_system.png")
        self.systray.setIcon(icon)
        self.systray.setVisible(True)
        self.systray.setToolTip("My running systray example")
        self.timer = QTimer()

        self._make_menu()
        self.systray.setContextMenu(self.menu)

    def _make_menu(self) -> None:
        self.menu = QMenu()
        self.tmr_single = QAction("Trigger one shot timer")
        self.tmr_periodic = QAction("Trigger periodic timer")
        self.tmr_stop = QAction("Stop timer")
        self.item_quit = QAction("Quit")

        self.menu.addAction(self.tmr_single)
        self.menu.addAction(self.tmr_periodic)
        self.menu.addAction(self.tmr_stop)
        self.menu.addAction(self.item_quit)

    def timer_single(self) -> None:
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.msg_timer_single)
        self.timer.start(3000)

    def msg_timer_single(self) -> None:
        msg = {
            "title": "Single shot timer", 
            "txt": "3 seconds elapsed", 
            "icon": "info"}
        print("Single timer finished")
        self.show_msg(msg)

    def timer_periodic(self) -> None:
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.msg_timer_periodic)
        self.timer.start(3000)

    def msg_timer_periodic(self) -> None:
        msg = {
            "title": "Periodic shot timer",
            "txt": "3 seconds elapsed",
            "icon": "warn",
        }
        print("Periodic timer finished")
        self.show_msg(msg)

    def timer_stop(self) -> None:
        self.timer.stop()

    def show_msg(self, msg: Dict) -> None:
        _icon = None
        if msg["icon"] == "info":
            _icon = QSystemTrayIcon.MessageIcon.Information
        elif msg["icon"] == "warn":
            _icon = QSystemTrayIcon.MessageIcon.Warning
        elif msg["icon"] == "error":
            _icon = QSystemTrayIcon.MessageIcon.Critical
        else:
            _icon = QSystemTrayIcon.MessageIcon.NoIcon
        self.systray.showMessage(msg["title"], msg["txt"], _icon, 1000)


def main():
    app = QApplication(sys.argv)
    systray = SysTray(app=app)
    systray.item_quit.triggered.connect(app.quit)
    systray.tmr_single.triggered.connect(systray.timer_single)
    systray.tmr_periodic.triggered.connect(systray.timer_periodic)
    systray.tmr_stop.triggered.connect(systray.timer_stop)
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
