#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__version__ = '1.3-1'

"""
Labsync Tray Qt
"""

import sys
import os
import signal
import atexit
import argparse
import dbus
from functools import partial
from enum import Enum
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import (QSystemTrayIcon, QApplication, QWidget, QMenu, qApp,
                            QWidget, QApplication, QMessageBox, QStyle)

# Import icons from resources.py (generated from resources.qrc)
# Command:
# pyrcc5 resources.qrc -o resources.py
import resources

"""
--------------------------------------------------------------------------------
    Config
--------------------------------------------------------------------------------
"""
APP_NAME = "Labsync Tray Qt"
APP_YEAR = "2024"
APP_VERSION = __version__ + '-' + APP_YEAR
APP_DESCR = "Notifica lo stato di aggiornamento del software nella cartella /opt"
APP_AUTHORS = """Sezione di Informatica - DiSIT
Università del Piemonte Orientale"""

# Not used in this version
#CURR_DIR = os.path.dirname(os.path.abspath(__file__))

# Status names
class AppStatus(Enum):
    APP_OFF = 'Off'
    APP_SYNC = "Syncing"
    APP_SYNCED = "Synced"


# Icons: could be PNG or ICO as well
APP_ICONS = {
    AppStatus.APP_OFF: ":/icons/off.png",
    AppStatus.APP_SYNC: ":/icons/syncing.png",
    AppStatus.APP_SYNCED: ":/icons/ok.png"
}

# Default milliseconds between checks
CHECK_INTERVAL = 15000

# Text messages and strings
MSG_SYNC_TITLE = "Aggiornamento software in corso"
MSG_SYNC_TEXT = """Stiamo aggiornando il software nella cartella /opt; ci \
sarà un po' di attività sul disco.

Alcuni dei programmi lì presenti (comprese le macchine virtuali) potrebbero \
non funzionare correttamente fino al termine degli aggiornamenti.

Il termine della procedura di aggiornamento ti verrà notificato, ti \
preghiamo di attendere qualche minuto.

<b>Se hai atteso più di mezz&apos;ora, non hai ricevuto la notifica e il \
software che vuoi usare continua a non funzionare, contatta i tecnici.</b>"""

MSG_SYNCED_TITLE = 'Software aggiornato'
MSG_SYNCED_TEXT = """Il software nella cartella
/opt è aggiornato."""

# File to check for saying if we are syncing or not
FILE_TO_CHECK = "/opt/ver"

# Strings
STR_STATUS = "Stato"
STR_ABOUT = "Info"
STR_EXIT = "Esci"

class SystemTrayIcon(QSystemTrayIcon):

    def __init__(self, icon, parent=None, check_interval=None):
        # Set defaults and parameters
        self.parent = parent
        self.status_changed = False
        self.status = AppStatus.APP_SYNCED
        self.check_interval = CHECK_INTERVAL
        
        if check_interval != None:
            self.check_interval = check_interval
        
        # dbus interface for notifications
        item = "org.freedesktop.Notifications"
        self.notify_interface = dbus.Interface(
            dbus.SessionBus().get_object(item, "/"+item.replace(".", "/")), item)
        
        QSystemTrayIcon.__init__(self, icon, parent)
        
        # Define menu items
        menu = QMenu(parent)
        statusAction = menu.addAction(STR_STATUS)
        statusAction.triggered.connect(self.statusActionHandler)
        aboutAction = menu.addAction(STR_ABOUT)
        aboutAction.triggered.connect(self.aboutActionHandler)
        exitAction = menu.addAction(STR_EXIT)
        exitAction.triggered.connect(qApp.quit)
        self.setContextMenu(menu)
        
        self.show()
        self.setToolTip(APP_NAME)

        self.timerHandler()

    def timerHandler(self):
        try:
            timer = QtCore.QTimer()
            timer.timeout.connect(self.timerHandler)
            timer.start(self.check_interval)
            self.updateStatus()
            if self.status_changed:
                self.updateIcon()
                self.sendNotification()
        finally:
            QtCore.QTimer.singleShot(self.check_interval, self.timerHandler)

    def statusActionHandler(self, evt):
        self.sendNotification()
    
    def aboutActionHandler(self, evt):
        self.showInfoBox()
    
    def showInfoBox(self):
        infoBox = QMessageBox()
        infoBox.setIcon(QMessageBox.Information)
        infoBox.setWindowTitle("About")
        infoBox.setText(APP_NAME + " " + APP_VERSION +"\n\n" +
                        APP_DESCR + "\n\n" +
                        "Realizzato da:\n" + APP_AUTHORS)
        wrapper = partial(center, infoBox)
        QtCore.QTimer.singleShot(0, wrapper)
        infoBox.exec_()
        return infoBox
        
    def updateIcon(self):
        self.setIcon(QtGui.QIcon(APP_ICONS[self.status]))

    def sendNotification(self):
        title = MSG_SYNCED_TITLE
        txt = MSG_SYNCED_TEXT
        #icon = QSystemTrayIcon.Information     
        if self.status == AppStatus.APP_SYNC:
            title = MSG_SYNC_TITLE
            txt = MSG_SYNC_TEXT
            #icon = QSystemTrayIcon.Warning
        else:
            title = MSG_SYNCED_TITLE
            txt = MSG_SYNCED_TEXT
            #icon = QSystemTrayIcon.Information
        
        ''' This does not work...
        icon = QtGui.QIcon(APP_ICONS[self.status])
        self.showMessage(title, txt, icon, 150000)
        '''
        # send notification using dbus interface
        self.notify_interface.Notify(APP_NAME, 0, "", title, txt, [], {"urgency": 1}, 150000)

    def updateStatus(self):
        exists = os.path.isfile(FILE_TO_CHECK)
        status = AppStatus.APP_OFF
        if exists:
            status = AppStatus.APP_SYNCED
        else:
            status = AppStatus.APP_SYNC
        if self.status != status:
            self.status_changed = True
            self.status = status
        else:
            self.status_changed = False

"""
--------------------------------------------------------------------------------
  Main
--------------------------------------------------------------------------------
"""
def handler_exit():
    cleanup()

def handler_signal(sig, frame):
    cleanup()

def cleanup():
    # Nothing to do in this version, but better keep this for future use
    pass

def center(window):
    # https://stackoverflow.com/a/57742440
    # https://wiki.qt.io/How_to_Center_a_Window_on_the_Screen

    window.setGeometry(
        QStyle.alignedRect(
            QtCore.Qt.LeftToRight,
            QtCore.Qt.AlignCenter,
            window.size(),
            qApp.desktop().availableGeometry(),
        )
    )

if __name__ == '__main__':
    # Parse command line argumentsimport sys
    parser = argparse.ArgumentParser(description=APP_NAME + ' ' + APP_VERSION)
    parser.add_argument('-i', '--interval', type=int,
        help='check interval (ms)')
    args = parser.parse_args()

    # Clean up on exit
    atexit.register(handler_exit) # normal exit
    signal.signal(signal.SIGINT, handler_signal) # ctrl-c
    signal.signal(signal.SIGTERM, handler_signal) # kill signal (but not kill -9)

    app = QApplication(sys.argv)
    app.setApplicationDisplayName(APP_NAME)

    # Avoid closing the app after closing the about box
    app.setQuitOnLastWindowClosed(False)

    # This is the parent of the QSystemTrayIcon object (it must be a QWidget object)
    w = QWidget()
    trayIcon = SystemTrayIcon(QtGui.QIcon(APP_ICONS[AppStatus.APP_SYNCED]), w, check_interval=args.interval)
    print("Check interval: " + str(trayIcon.check_interval) + "ms")
    #trayIcon.show()
    sys.exit(app.exec_())
