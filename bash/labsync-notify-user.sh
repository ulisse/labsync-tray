#!/bin/bash
# send a notification to the logged in user (graphical session only)

# seconds between checks
CYCLE_PERIOD=30
# how long the notification stays on screen (milliseconds, 0=until user clicks)
NOTIFY_TIME=0
# messages
SYNC_RUN_TITLE="labsync"
SYNC_RUN_MESSAGE="Stiamo aggiornando il software nella cartella /opt; ci sarà un po&apos; di attività sul disco.\
\r\rAlcuni dei programmi lì presenti \(comprese le macchine virtuali\) potrebbero non funzionare correttamente fino al termine degli aggiornamenti.\
\r\rIl termine della procedura di aggiornamento ti verrà notificato, ti preghiamo di attendere qualche minuto.\
\r\r<b>Se hai atteso più di mezz&apos;ora, non hai ricevuto la notifica e il software che vuoi usare continua a non funzionare, contatta i tecnici.</b>"
SYNC_END_TITLE='labsync'
SYNC_END_MESSAGE="Il software nella cartella /opt è aggiornato."

# folder for temporary files
TMP=/var/tmp

# task names (just for the lock file name)
SYNC_TASK="sync"
END_TASK="end"

xuser=$USER
xuid=""

createTmpDir() {
    if [[ ! -e ${TMP} ]]; then
        mkdir -p ${TMP}
    fi
}

getUserVars() {
    if [[ "${xuser}" != "" ]]; then
            # detect the id of the user
            xuid=$(id -u ${xuser})
    fi
}

isSyncActive() {
    if [[ -e /opt/ver ]]; then
        echo "0"
    else
        echo "1"
    fi
}

sendNotification() {
    title=$1
    message=$2
    icon=$3
    category=$4
    urgency=$5
    task=$6
#    if [[ "$xuser" != "" ]]; then
        if [[ ! -e ${TMP}/labsync-notify-${xuid}-${task} ]]; then
            touch ${TMP}/labsync-notify-${xuid}-${task}
            notify-send \
                -t ${NOTIFY_TIME} -u ${urgency} -i ${icon} -c ${category} "${title}" "${message}"
        fi
#    fi
}

cleanTmpFiles() {
    rm ${TMP}/labsync-notify-*-${SYNC_TASK} > /dev/null 2>&1
    rm ${TMP}/labsync-notify-*-${END_TASK} > /dev/null 2>&1
}

################################################################################

# create the folder for temporary files
createTmpDir

# cycle until labsync task is running
while [[ $(isSyncActive) == "1" ]]; do
    getUserVars
    sendNotification "${SYNC_RUN_TITLE}" "${SYNC_RUN_MESSAGE}" dialog-warning transfer critical ${SYNC_TASK}
    sleep ${CYCLE_PERIOD}
done

# check if a user is still logged in
getUserVars

# show completion message only if user was notified
if [[ -e ${TMP}/labsync-notify-${xuid}-${SYNC_TASK} ]]; then
    sendNotification "${SYNC_END_TITLE}" "${SYNC_END_MESSAGE}" info transfer.complete normal ${END_TASK}
fi

cleanTmpFiles
