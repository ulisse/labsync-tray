#!/usr/bin/env bash

##########
# CONFIG ###############################################################
########################################################################
BACKUP_PATH=~/pCloudDrive/
EXCLUDE=('.git' 'public' 'node_modules' 'venv')

##########
# FUNCTIONS ############################################################
########################################################################
ANAME=${PWD##*/}
T=`date +"%Y-%m-%d_%H%M"`
FILENAME=${ANAME}_${T}

# https://askubuntu.com/a/1386907
function choose_from_menu() {
    local prompt="$1" outvar="$2"
    shift
    shift
    local options=("$@") cur=0 count=${#options[@]} index=0
    local esc=$(echo -en "\e") # cache ESC as test doesn't allow esc codes
    printf "$prompt\n"
    while true
    do
        # list all options (option list is zero-based)
        index=0 
        for o in "${options[@]}"
        do
            if [ "$index" == "$cur" ]
            then echo -e " >\e[7m$o\e[0m" # mark & highlight the current option
            else echo "  $o"
            fi
            index=$(( $index + 1 ))
        done
        read -s -n3 key # wait for user to key in arrows or ENTER
        if [[ $key == $esc[A ]] # up arrow
        then cur=$(( $cur - 1 ))
            [ "$cur" -lt 0 ] && cur=0
        elif [[ $key == $esc[B ]] # down arrow
        then cur=$(( $cur + 1 ))
            [ "$cur" -ge $count ] && cur=$(( $count - 1 ))
        elif [[ $key == "" ]] # nothing, i.e the read delimiter - ENTER
        then break
        fi
        echo -en "\e[${count}A" # go up to the beginning to re-render
    done
    # export the selection to the requested output variable
    printf -v $outvar "${options[$cur]}"
}

##########
# MAIN #################################################################
########################################################################

#echo "Linux "$T > version.txt

selections=(
"TAR + 7ZIP"
"TAR + GZIP"
"RAR"
"7ZIP"
)

choose_from_menu "Tipo archivio:" selected_choice "${selections[@]}"
echo "Archivio selezionato: $selected_choice"
echo
echo "EXCLUDE: ${EXCLUDE[@]}"
echo
read -p "Premi un tasto per continuare o CTRL-C per interrompere"

case $selected_choice in
    "TAR + 7ZIP")
        FILENAME+=".tar.7z"
        exclude_options=()
        for x in "${EXCLUDE[@]}"; do
            exclude_options+=(--exclude="$x")
        done
        tar "${exclude_options[@]}" -cvf - ../"${ANAME}"/ | 7z a -si ../"${FILENAME}"
        ;;
    "TAR + GZIP")
        FILENAME+=".tgz"
        exclude_options=()
        for x in "${EXCLUDE[@]}"; do
            exclude_options+=(--exclude="$x")
        done
        tar "${exclude_options[@]}" -cvzf ../"${FILENAME}" ../"${ANAME}"
        ;;
    "RAR")
        FILENAME+=".rar"
        exclude_options=()
        for x in "${EXCLUDE[@]}"; do
            exclude_options+=(-x"../${ANAME}/$x")
        done
        rar a -r -m5 ../"${FILENAME}" ../"${ANAME}"/ "${exclude_options[@]}"
        ;;
    "7ZIP")
        FILENAME+=".7z"
        exclude_options=()
        for x in "${EXCLUDE[@]}"; do
            exclude_options+=(-x"!${ANAME}/$x")
        done
        7z a "${exclude_options[@]}" ../"${FILENAME}" ../"${ANAME}"/
        ;;
esac      

echo
read -p "Sposta su disco di backup? (s/n)? " answer
case ${answer:0:1} in
    s|S )
        mv ../${FILENAME} ${BACKUP_PATH}
    ;;
    * )
    ;;
esac
