#!/usr/bin/python
# -*- coding: utf-8 -*-

import signal
import os

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Notify
from gi.repository import Gtk

import subprocess

################################################################################
# CONFIG
################################################################################
APP_NAME = "labsync-tray"
APP_VERSION = "0.1-2019"
APP_DESCR = "Notifica lo stato di aggiornamento del software nella cartella /opt"
APP_AUTHORS = ['Sezione di Informatica, DiSIT, UPO']
APP_ID = "labsync-tray"

CURR_DIR = os.path.dirname(os.path.abspath(__file__))

# could be PNG or SVG as well
APP_ICON = os.path.join(CURR_DIR, 'sample_icon.svg')

# force using StatusIcon over AppIndicator
FORCE_TRAY = True
################################################################################
################################################################################

class TrayApp:

    def __init__(self, appid, icon, menu=None):
        if menu != None:
            self.menu
        else:
            self.menu = self.build_menu()
        self.menu.show_all()

        APPIND_SUPPORT = 1
        try:
            from gi.repository import AppIndicator3
        except:
            APPIND_SUPPORT = 1

        if APPIND_SUPPORT == 1 and FORCE_TRAY == False:
            self.ind = AppIndicator3.Indicator.new(
                appid, icon, AppIndicator3.IndicatorCategory.APPLICATION_STATUS)
            self.ind.set_status(AppIndicator3.IndicatorStatus.ACTIVE)
            self.ind.set_menu(self.menu)
        else:
            self.ind = Gtk.StatusIcon()
            self.ind.set_visible(True);
            self.ind.connect('popup-menu', self.onPopupMenu)
            self.ind.set_tooltip_text(APP_NAME)
            self.ind.set_title(APP_NAME)
            if icon != "":
                self.ind.set_from_file(icon)
            else:
                self.ind.set_from_stock(Gtk.STOCK_INFO)

    def onPopupMenu(self, icon, button, time):
        self.menu.popup(None, None, Gtk.StatusIcon.position_menu, icon, button, time)

    def build_menu(self):
        menu = Gtk.Menu()

        item_status = Gtk.MenuItem('Status')
        item_status.connect("activate", self.handler_menu_status)
        menu.append(item_status)

        about = Gtk.MenuItem("About")
        about.show()
        about.connect("activate", self.handler_menu_about)
        menu.append(about)

        menu.append(Gtk.SeparatorMenuItem())

        item_quit = Gtk.MenuItem("Quit")
        item_quit.connect("activate", self.handler_menu_quit)
        menu.append(item_quit)
        return menu

    def handler_menu_quit(self, evt):
        Notify.uninit()
        Gtk.main_quit()

    def handler_menu_status(self, evt):
        Notify.Notification.new("labsync", "<b>Test</b> message").show()

    def handler_menu_about(self, evt):
        about_dialog = Gtk.AboutDialog()
        about_dialog.set_destroy_with_parent(True)
        about_dialog.set_icon_name("dialog-information")
        # no SVG?
        about_dialog.set_logo_icon_name("")
        about_dialog.set_program_name(APP_NAME)
        about_dialog.set_name(APP_NAME)
        about_dialog.set_version(APP_VERSION)
        about_dialog.set_license_type(Gtk.License.GPL_3_0)
        about_dialog.set_comments(APP_DESCR)
        about_dialog.set_authors(APP_AUTHORS)
        about_dialog.set_default_response(Gtk.ResponseType.CLOSE)
        print APP_ICON
        about_dialog.run()
        about_dialog.destroy()

    def main(self):
        Gtk.main()

if __name__ == "__main__":
    # Handle pressing Ctr+C properly, ignored by default
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = TrayApp(APP_ID, APP_ICON)
    Notify.init(APP_ID)
    app.main()
