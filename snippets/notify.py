#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__version__ = '0.1'

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Notify, Gtk, GLib

from urllib.request import FancyURLopener
from datetime import datetime, date, time, timedelta
import os.path, sys, getopt
from subprocess import call

serialisedvalue=0;
serialiseddate=0;

def callback():
        call(["chromium", "gmail.com"])

def serialise(unread):
        try:
                f = open("mailcount", "w")
                try:
                        f.write(unread+"\n") # Write a string to a file
                        f.write(datetime.now().strftime('%b %d %Y %I:%M%p'))
                finally:
                        f.close()
        except IOError:
                pass

def deserialise():
        global serialisedvalue
        global serialiseddate
        try:
                f = open("mailcount", "r")
                try:
                        serialisedvalue = f.readline().rstrip()
                        serialiseddate = datetime.strptime(f.readline(), '%b %d %Y %I:%M%p')
                finally:
                        f.close()
        except IOError:
                pass

def notif(unread):
        Notify.init ("New Mail")
        if unread != "1":
                Hello=Notify.Notification.new ("New mail","You have "+unread+" unread mails","/usr/share/icons/Faenza/actions/96/mail-forward.png")
        else :
                Hello=Notify.Notification.new ("New mail","You have "+unread+" unread mails","/usr/share/icons/Faenza/actions/96/mail-forward.png")
        Hello.add_action('action', 'Read', callback, None, None)
        Hello.show ()

def main(argv):

        notify=0
        forced=0
        try:
                opts, args = getopt.getopt(argv,"nf",['notify','force-notify'])
        except getopt.GetoptError:
                print("unreadgmail.py [-n --notify] [-f --force-notify")
                sys.exit(2)
        for opt,args in opts:
                if opt in ("-n", "--notify"):
                        notify=1
                elif opt in ("-f","--force-notify"):
                        forced=1
        #url = 'https://%s:%s@mail.google.com/mail/feed/atom' % ("myaccount", "mypassword")
        url = 'https://%s:%s@mail.google.com/mail/feed/atom' % ("ulisse.alb", "oklahoma:2014")
        opener = FancyURLopener()
        page = opener.open(url)
        contents = page.read().decode('utf-8')
        ifrom = contents.index('<fullcount>') + 11
        ito   = contents.index('</fullcount>')
        unread = contents[ifrom:ito]
        print("Unread messages : "+unread)
        if notify==1 and forced==0:
                if os.path.exists("mailcount"):
                        deserialise()
                else:
                        serialise(unread)
                        deserialise()
                if unread != "0":
                        if unread != serialisedvalue:
                                notif(unread)
                                serialise(unread)
                        elif ((datetime.now() - serialiseddate) > timedelta(hours=1)):
                                notif(unread)
        if forced==1:
                notif(unread)
        GLib.MainLoop().run()




if __name__ == "__main__":
   main(sys.argv[1:])
