#!/bin/bash
# https://ubuntuforums.org/showthread.php?t=910717
# https://www.devdungeon.com/content/debian-package-tutorial-dpkgdeb
# https://askubuntu.com/questions/735883/what-user-file-owner-to-use-when-creating-debian-packages

#if [[ "z$1" == "z" ]]; then
#	echo "Uso: $0 <dir>"
#	exit 1
#fi

VERSION=1.3-1
mkdir labsync-tray_${VERSION}
cd labsync-tray_${VERSION}
install -d -m 0755 etc/xdg/autostart
install -d -m 0755 usr/local/sbin
install -d -m 0755 usr/share/doc/labsync-tray
cd ..

cp src/labsync-tray-qt.py labsync-tray_${VERSION}/usr/local/sbin
chmod 0755 labsync-tray_${VERSION}/usr/local/sbin/labsync-tray-qt.py

cp src/resources.py labsync-tray_${VERSION}/usr/local/sbin
chmod 0644 labsync-tray_${VERSION}/usr/local/sbin/resources.py

cp src/labsync-tray.desktop labsync-tray_${VERSION}/etc/xdg/autostart
chmod 0644 labsync-tray_${VERSION}/etc/xdg/autostart/labsync-tray.desktop

cp -r DEBIAN labsync-tray_${VERSION}

cp doc/* labsync-tray_${VERSION}/usr/share/doc/labsync-tray
gzip -n --best labsync-tray_${VERSION}/usr/share/doc/labsync-tray/LICENSE
gzip -n --best labsync-tray_${VERSION}/usr/share/doc/labsync-tray/changelog

cd labsync-tray_${VERSION}
md5sum usr/share/doc/labsync-tray/LICENSE.gz >> DEBIAN/md5sums
md5sum usr/share/doc/labsync-tray/changelog.gz >> DEBIAN/md5sums
cd ..

dpkg-deb --root-owner-group --build labsync-tray_${VERSION}

echo
read -p "Elimino la cartella temporanea? " answer
case ${answer:0:1} in
    s|S )
        rm -rf labsync-tray_${VERSION}
    ;;
    * )
    ;;
esac

echo Done!
