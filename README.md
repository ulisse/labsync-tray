# LabSync Tray
L'applicazione notifica all'utente lo stato del servizio labsync (sincronizzazione in corso / aggiornato).

## Versioni
Esistono 3 versioni dell'applicazione:
* pure bash
* systemd
* Python 3 (Qt 5, GTK 3 e wxWindows)

### systemd
La versione systemd è un tentativo iniziale poco evoluto. Risulta scomodo in quanto deve girare con privilegi di root per capire l'utente che ha la sessione grafica aperta per potergli inviare le notifiche (con sendNotification).
Esiste anche un tentativo di far girare il servizio con i privilegi dell'utente che entra nella sessione grafica, ma non funziona.

### bash
La versione bash ha il suo lanciatore che può essere copiato in /etc/xdg/autostart e risolve il problema precedente in quanto gira con i privilegi dell'utente che ha fatto login.
Lo script invia le notifiche usando sendNotification ed è pienamente funzionante.

### Python
Le 3 versioni Python sono piccole applicazioni che possono essere lanciate all'avvio della sessione grafica (come la versione bash) e rimangono attive nella system tray, con un'icona di stato che indica l'attività di labsync. Viene inoltre inviata una notifica con un messaggio, come le versioni precedenti. E' presente inoltre un piccolo menu che offre la possibilità di mostrare il messaggio con lo stato corrente, avere info sull'applicazione e terminarla.
La versione Qt 5 (labsync-tray-qt.py) è quella più aggiornata ed estesa. La versione GTK 3 (labsync-tray.py) è parzialmente funzionante (non mostra l'icona corretta nella system tray) mentre quella wxWindows (labsync-tray-wx.py) è un tentativo di non usare GTK, ma è meno compatibile. Sia la versione GTK 3 che wxWindows non sono più aggiornate.

## Funzionamento
Tutte le versioni controllano la presenza del file /opt/ver ad intervalli regolari (default 30 secondi, impostabile da linea di comando nella versione Python attraverso il flag -i). Nel caso in cui manchi questo file, si presume che labsync stia girando, in caso contrario che il software in /opt sia aggiornato.
In mancanza di un reale controllo della versione corrente del software sul server, questo è il solo meccanismo utilizzato per determinare lo stato della sincronizzazione.

## Avvertenze
<strong>Con GTK 4 il supporto alle applicazioni nella system tray sarà rimosso</strong> (ora è solo deprecato). Già nella versione corrente di GNOME non è più visibile alcuna icona di applicazione (se non quelle di GNOME stesso). In quel momento quindi anche le librerie wrapper di GTK per Python non supporteranno più questa modalità e sarà necessario rimuovere il codice obsoleto. Le notifiche in ogni caso dovrebbero continuare a funzionare, visto che sono anche il modo in cui gli sviluppatori di GTK raccomandano di comunicare con l'utente al posto di usare un'icona di stato. Si perderà comunque il menu, sostituito eventualmente da bottoni presenti nelle notifiche stesse. Nella versione GTK è stato già aggiunto un bottone a tutte le notifiche che mostra la finestra di informazioni dell'applicazione (potrebbe essere usato per comunicare altre cose all'utente).

## Preparazione pacchetto RPM
Nella cartella <strong>rh/specs</strong> è contenuto il file per la generazione del pacchetto RPM.

Dopo aver scaricato il pacchetto .tar.gz dal repository GIT, copiarlo nella propria cartella ~/rpmbuild/SOURCES. Copiare il file .spec nella propria cartella ~/rpmbuild/SPECS e lanciare:

`rpmbuild -bb --clean $HOME/rpmbuild/SPECS/labsync-tray.spec`
